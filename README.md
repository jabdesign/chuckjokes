#Frontmen Assignment
Jabran Saeed

###Disclaimer
To keep the dev time manageable, I've skipped some tasks that would normally be essential. Webpack/Babel setups are minimal. No tests either since they were not explicitly demanded. No linters or pretifiers. Some shortcuts with boilerplate (eg no central definition of action strings for redux).

###Scalibility
I have invested in redux based state management to satisfy the scalibility factor of the assignment. If need be, redux architecture of the app can be easily modified to cater additional complexity.

###What I could do better
No css frameworks were used and the css itself is quite crude but does the job. A Chuck Norris roundhouse kick spritesheet for a loader would be hilarous..

###End