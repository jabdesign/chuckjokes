import React from 'react';
import Toolbar from '../containers/Toolbar';
import {Row, Col} from 'react-flexbox-grid';
export default class TopHeader extends React.Component {
    render() {
        return (<div className="topheader">
        <Row>
            <Col xs={3} md={4} lg={6}>
               <Title/>
            </Col>
            <Col xs={9} md={8} lg={6}>
                <Toolbar/>
            </Col>
        </Row></div>);
    }
}

const Title = () => {
    return (<div className="title">
        <h1>Chuck</h1>
        <h1>Norris</h1>
        <h1>Jokes</h1>
    </div>)
}