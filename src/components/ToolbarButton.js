import React from 'react';
const ToolbarButton = (props) => {
    return (<div className="toolbar-button" onClick={props.onClick}>{props.text}</div>);
}

export default ToolbarButton;