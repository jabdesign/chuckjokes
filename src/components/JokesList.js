import React from 'react';
import {Col} from 'react-flexbox-grid';
const JokesList = (props) => {
    return (<Col xs={6}>
        <div className="jokeslist">
        <ul>
        {
            props.jokes_list.map( (j) => {
                return <li className={j.isFav ? 'selected' : ''} key={j.id} onClick={() => props.onClick(j)}>{j.joke.toString()}</li>;
            })
        }
        {
            props.isLoading && <div className="message">Loading ...</div>
        }
        </ul>
        </div>
    </Col>);
}

export default JokesList;