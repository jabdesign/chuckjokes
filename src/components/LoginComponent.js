import React from 'react';
import {Row, Col} from 'react-flexbox-grid';
import Modal from 'react-modal';
import ToolbarButton from './ToolbarButton';
import {validatePassword} from '../utils/cleaner';

class LoginComponent extends React.Component {
    constructor(props) {
        super(props);
        this.handleInput = this.handleInput.bind(this);
        this.submit = this.submit.bind(this);
        this.state = {
            username: '',
            password: ''
        };
    }
    handleInput(type,value) {
        this.setState({
            [type]: value
        });
    }
    submit() {
        let {username, password} = this.state;
        validatePassword(password, (isValid, error) => {
            if(isValid)
               return this.props.login({
                    username,
                    password
                });
            alert(error);
        });
        
    }
    render() {
        return (<Modal
          isOpen={this.props.open}
          onRequestClose={this.props.loginModal}
          contentLabel="Login"
          style={{
              content: {
                  width:'50%',
                  left:'25%',
                  height:'300px',
              },
              overlay: {
                  backgroundColor: 'rgba(0,0,0,.75)'
              }
          }}
        >
            <div className="login">
                <div className="title">Login</div>
                <input type="text" placeholder="Username" onChange={(e) => { this.handleInput('username',e.target.value) }}/>
                <input type="text" placeholder="Password" onChange={(e) => { this.handleInput('password',e.target.value) }}/>
                <div>
                <div className="flex center">
                    <ToolbarButton onClick={this.submit} text="Ok"/>
                    <ToolbarButton onClick={this.props.loginModal} text="Cancel"/>
                </div>
                </div>
            </div>
        </Modal>)
    }
}

export default LoginComponent;