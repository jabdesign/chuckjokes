import React from 'react';
import {Col} from 'react-flexbox-grid';
const Favourites = (props) => {
    return (<Col xs={6}>
                <div className="jokeslist">
                <ul>
                {
                    props.favourites.length > 0 && props.favourites.map( (j) => {
                        return <li onClick={props.onClick.bind(null, j)} key={j.id}>{j.joke.toString()}</li>;
                    })
                }
                </ul>
                {
                    props.favourites.length === 0 && props.jokes_list.length > 0 && <div className="message">Select your favourite jokes</div>
                }
                {
                    props.favourites.length === 0 && props.jokes_list.length === 0 && <div className="message">Press reload for new jokes</div>
                }
                </div>
            </Col>);
}

export default Favourites;