import React from "react";
import { render } from 'react-dom'
import { createStore , applyMiddleware} from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import { PersistGate } from 'redux-persist/integration/react'
import reducer from './reducers'
import style from './styles/app.scss';
import App from './App.js';

const persistConfig = {
  key: 'root',
  storage,
  blacklist: ['loggedIn']
}
const persistedReducer = persistReducer(persistConfig, reducer)
const store = createStore(persistedReducer, applyMiddleware(thunk))
const persistor = persistStore(store)
render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <App />
    </PersistGate>
  </Provider>,
  document.getElementById('root')
)