import React from 'react';
import TopHeader from './components/TopHeader.js';
import JokesComponent from './containers/JokesComponent.js';
export default class App extends React.Component {
    render() {
        return (<div>
            <TopHeader/>
            <JokesComponent/>
        </div>);
    }
}