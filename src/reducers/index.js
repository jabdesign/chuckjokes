import { cleanStringQuotes } from '../utils/cleaner';
import Cookies from 'universal-cookie';
const cookies = new Cookies();
const initialState = {
    jokes_list: [],
    favourites: [],
    isLoading: false,
    loggedIn: cookies.get('username') || false
}
const chuckJokes = (state = initialState, action) => {
    switch(action.type) {
        case 'GET_JOKES':
            return {...state, jokes_list: [], isLoading: true};
        case 'GOT_JOKES':
            return { ...state,
                jokes_list: cleanStringQuotes(action.data),
                isLoading: false
            }
        case 'GOT_JOKE': {
            if(state.jokes_list.length === 10)
                return {...state};
            return {...state, jokes_list: state.jokes_list.concat(action.data)};
        }
        case 'MARK_FAVOURITE':  {
            let favourite_exists = state.favourites.find( (joke) => {
                return joke.id === action.data.id;
            });
            if(favourite_exists)
                return {...state, favourites: state.favourites.filter ( ( joke ) => {
                    return joke.id != action.data.id;
                })}
            return {...state, favourites: state.favourites.concat(action.data)};
        }
        case 'CLEAR_JOKES':
            return {...initialState, loggedIn: state.loggedIn};

        case 'ERROR_JOKES':
            return {...state, isLoading: false};
        case 'LOGIN': {
            cookies.set('username', action.data.username, { path: '/' });
            return {...state, loggedIn: true};
        }
        case 'LOGOUT': {
            cookies.remove('username');
            return {...state, loggedIn: false};
        }
        default:
            return state;
    }
}

export default chuckJokes;