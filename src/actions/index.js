export const GET_JOKES = () => {
    return dispatch => {
       dispatch(REQUEST_JOKES());
       fetch('http://api.icndb.com/jokes/random/10')
            .then(response => response.json())
            .then( (response) => {
                dispatch(GOT_JOKES(response.value));
            })
            .catch( (err) => {
                dispatch(ERROR_JOKES(err));
            });
    }
}

export const GET_JOKE = () => {
    return dispatch => {
        return fetch('http://api.icndb.com/jokes/random/1')
            .then( response => response.json())
            .then( (response) => {
                dispatch(GOT_JOKE(response.value))
            })
            .catch( (err) => {
                dispatch(ERROR_JOKE(err));
            });
    }
}

const REQUEST_JOKES = () => {
    return {
        type:'GET_JOKES'
    };
}

export const GOT_JOKES = (jokes) => {
    return {
        data: jokes,
        type:'GOT_JOKES'
    };
}

export const GOT_JOKE = (joke) => {
    return {
        data: joke,
        type:'GOT_JOKE'
    };
}

export const ERROR_JOKES = (err) => {
    return {
        type:'ERROR_JOKES'
    };
}

export const MARK_FAVOURITE = (params) => {
    return {
        type: 'MARK_FAVOURITE',
        data:params
    };
}

export const CLEAR_JOKES = () => {
    return {
        type: 'CLEAR_JOKES'
    };
}

export const LOGIN = (params) => {
    return {
        type: 'LOGIN',
        data:params
    };
}

export const LOGOUT = () => {
    return {
        type: 'LOGOUT'
    };
}