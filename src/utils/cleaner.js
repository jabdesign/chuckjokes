import {series} from 'async';
import {uniq} from 'lodash';

export const cleanStringQuotes = (arr=[]) => {
    return arr.map( (a) => {
        return {...a, joke: a.joke.replace(/&quot;/g, '\"')};
    })
}

export const syncJokesToFavourites = (state) => {
    let {jokes_list, favourites} = state;
    if(favourites.length === 0)
        return state;
    let favs = favourites.map( (f) => { return f.id; });
    let syncdJokes = jokes_list.map( (j) => {
        // Does the id is favourite?
        if(favs.indexOf(j.id) >= 0)
            return {...j, isFav: true};
        return {...j, isFav: false};
    });
    return {...state, jokes_list: syncdJokes};
}

export const validatePassword = (password, callback) => {
    let tasks = [
        bannedLetter,
        maxLength,
        caseSensitivity,
        straighTriplets,
        nonOverlappingPairs
    ];
    tasks = tasks.map ( (t) => {
        return t.bind(null, password);
    });
    return series(tasks, function(err, response) {
        if(err)
            return callback(false, err);
        return callback(true);
    })
}

/*
    PASSWORD VALIDATORS

*/

function bannedLetter(payload, cb) {
    let banned = ['i', 'O', 'l'];
    let result = banned.every( (letter) => {
        return payload.indexOf(letter) < 0;
    });
    if(result)
        return cb(null, true);
    return cb('Banned Letter Error', null);
}

function maxLength(payload, cb) {
    if(payload.length > 32)
        return cb('Max Length Error', null);
    return cb(null, true);
}

function caseSensitivity(payload, cb) {
    let result = payload.split('').every( (p) => {
        if(!isNaN(p))
            return true;
        if(p === p.toLowerCase())
            return true;
        return false;
    });
    if(result)
        return cb(null, true);
    return cb('Case Sensitivity Error', null);
}

var ENGLISH = "abcdefghijklmnopqrstuvwxyz".split("");

function straighTriplets(payload, cb) {
    // convert alphabets to numerical order
    let password = payload.split('').map( (p) => {
        return ENGLISH.indexOf(p);
    })
   // sliding window iterator
    let combinations = 0, i = 0;
    while(i <= password.length - 3) {
        // run a test on each triplet
        let letters = password.slice(i, i+3);
        if(letters[2] - letters[1] === 1 && letters[1] - letters[0] === 1)
            ++combinations;
        i++;

    }
    if(combinations>0)
        return cb(null, true);
    return cb('Straight Triplets Error', null);
}

function nonOverlappingPairs(payload, cb) {
    const regex = /(\w)\1/g;
    let pairs = _.uniq(payload.match(regex));
    if(pairs.length >= 2)
        return cb(null, true);
    return cb('Non Overlapping Pairs Error', null);
}