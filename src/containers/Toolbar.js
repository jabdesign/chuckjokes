import React from 'react';
import { connect } from 'react-redux'
import { GET_JOKES, CLEAR_JOKES, GET_JOKE, LOGIN, LOGOUT } from '../actions';
import {Row, Col} from 'react-flexbox-grid';
import LoginComponent from '../components/LoginComponent';
import ToolbarButton from '../components/ToolbarButton';
class Toolbar extends React.Component {
    constructor(props) {
        super(props);
        this.clearJokes = this.clearJokes.bind(this);
        this.reload = this.reload.bind(this);
        this.timeBased = this.timeBased.bind(this);
        this.loginModal = this.loginModal.bind(this);
        this.loginHandler = this.loginHandler.bind(this);
        this.logout = this.logout.bind(this);
        this.state = {
            open: false
        };
    }
    clearJokes() {
        this.props.clearJokes();
    }
    reload() {
        this.props.getJokes();
    }
    timeBased() {
        if(this.props.jokes_list.length >= 10)
            return;
        setTimeout(() => {
            this.props.getJoke();
            this.timeBased();
        }, 1000);
    }
    loginModal() {
        this.setState({
            open: !this.state.open
        });
    }
    loginHandler(params) {
        this.setState({
            open: false
        }, () => {
            this.props.login(params);
        });
    }
    logout() {
        this.props.logout();
    }
    render() {
        return (<div className="toolbar">
        <Row>
            <Col xs={12}>
                <Row end="xs">
                    <Col xs>
                        <ToolbarButton onClick={this.clearJokes} text="Clear"/>
                    </Col>
                    <Col xs>
                        <ToolbarButton onClick={this.reload} text="Reload"/>
                    </Col>
                    <Col xs>
                        <ToolbarButton onClick={this.timeBased} text="Time Based"/> 
                    </Col>
                    <Col xs>
                        { 
                            this.props.loggedIn && <ToolbarButton onClick={this.logout} text="Logout"/>
                        }
                        {
                            !this.props.loggedIn && <ToolbarButton onClick={this.loginModal} text="Login" />
                        }
                    </Col>
                </Row>
            </Col>
        </Row>
        <LoginComponent loginModal={this.loginModal} login={this.loginHandler} open={this.state.open}/>
        </div>);
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        getJokes: () => dispatch(GET_JOKES()),
        clearJokes: () => dispatch(CLEAR_JOKES()),
        getJoke: () => dispatch(GET_JOKE()),
        login: (params) => dispatch(LOGIN(params)),
        logout: () => dispatch(LOGOUT())
    }
}

const mapStateToProps = (state) => { return state };

export default connect(mapStateToProps, mapDispatchToProps)(Toolbar);