import React from 'react';
import { connect } from 'react-redux'
import { GET_JOKES, MARK_FAVOURITE } from '../actions';
import {Row, Col, Grid} from 'react-flexbox-grid';
import JokesList from '../components/JokesList';
import Favourites from '../components/Favourites';
import {syncJokesToFavourites} from '../utils/cleaner';

class JokesComponent extends React.Component {
    constructor(props) {
        super(props);
        this.jokeHandler = this.jokeHandler.bind(this);
    }

    componentDidMount() {
        if(this.props.jokes_list.length === 0)
            this.props.getJokes();
    }

    jokeHandler(params) {
        console.log(params);
        this.props.markFavourite(params);
    }

    render() {
        return (<div className="jokescomponent">
            <Grid fluid>
            <Row>
                <JokesList {...this.props} onClick={this.jokeHandler}/>
                <Favourites {...this.props} onClick={this.jokeHandler}/>
            </Row>
            </Grid>
        </div>);
    }
}

const mapStateToProps = (state) => { return syncJokesToFavourites(state); };
const mapDispatchToProps = (dispatch, props) => {
    return {
        getJokes: () => dispatch(GET_JOKES()),
        markFavourite: (props) => dispatch(MARK_FAVOURITE(props))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(JokesComponent);